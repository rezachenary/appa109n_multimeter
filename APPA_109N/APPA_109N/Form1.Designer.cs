﻿namespace APPA_109N
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnClearReceive = new System.Windows.Forms.Button();
            this.btnClearSend = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnApply = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.tbInterval = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.richReceive = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.richSend = new System.Windows.Forms.RichTextBox();
            this.comboBoxComPort = new System.Windows.Forms.ComboBox();
            this.btnConnect = new System.Windows.Forms.Button();
            this.panelLog = new System.Windows.Forms.Panel();
            this.btnStartLog = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdRewrite = new System.Windows.Forms.RadioButton();
            this.rdAppend = new System.Windows.Forms.RadioButton();
            this.tbPath = new System.Windows.Forms.TextBox();
            this.SerialPort = new System.IO.Ports.SerialPort(this.components);
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.lblSub = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.lblMain = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.lblRange = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.lblKey = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.lblRotor = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lblBlue = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.cbLog = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panelLog.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnClearReceive);
            this.panel1.Controls.Add(this.btnClearSend);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.comboBoxComPort);
            this.panel1.Controls.Add(this.btnConnect);
            this.panel1.Location = new System.Drawing.Point(12, 38);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1976, 1263);
            this.panel1.TabIndex = 2;
            // 
            // btnClearReceive
            // 
            this.btnClearReceive.Location = new System.Drawing.Point(1846, 497);
            this.btnClearReceive.Name = "btnClearReceive";
            this.btnClearReceive.Size = new System.Drawing.Size(116, 45);
            this.btnClearReceive.TabIndex = 2;
            this.btnClearReceive.Text = "clear";
            this.btnClearReceive.UseVisualStyleBackColor = true;
            this.btnClearReceive.Click += new System.EventHandler(this.btnClearReceive_Click);
            // 
            // btnClearSend
            // 
            this.btnClearSend.Location = new System.Drawing.Point(1846, 108);
            this.btnClearSend.Name = "btnClearSend";
            this.btnClearSend.Size = new System.Drawing.Size(116, 45);
            this.btnClearSend.TabIndex = 2;
            this.btnClearSend.Text = "clear";
            this.btnClearSend.UseVisualStyleBackColor = true;
            this.btnClearSend.Click += new System.EventHandler(this.btnClearSend_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 497);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 32);
            this.label2.TabIndex = 1;
            this.label2.Text = "receive";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel5.Controls.Add(this.btnApply);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.tbInterval);
            this.panel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.panel5.Location = new System.Drawing.Point(374, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(576, 124);
            this.panel5.TabIndex = 0;
            this.panel5.Paint += new System.Windows.Forms.PaintEventHandler(this.panel5_Paint);
            // 
            // btnApply
            // 
            this.btnApply.Location = new System.Drawing.Point(433, 50);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(136, 50);
            this.btnApply.TabIndex = 5;
            this.btnApply.Text = "Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 1);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(238, 32);
            this.label4.TabIndex = 4;
            this.label4.Text = "time Interval ms:";
            // 
            // tbInterval
            // 
            this.tbInterval.Location = new System.Drawing.Point(9, 57);
            this.tbInterval.Name = "tbInterval";
            this.tbInterval.Size = new System.Drawing.Size(418, 38);
            this.tbInterval.TabIndex = 3;
            this.tbInterval.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbInterval_KeyPress);
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.richReceive);
            this.panel3.Location = new System.Drawing.Point(21, 542);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1941, 720);
            this.panel3.TabIndex = 2;
            // 
            // richReceive
            // 
            this.richReceive.BackColor = System.Drawing.SystemColors.Info;
            this.richReceive.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richReceive.Location = new System.Drawing.Point(0, 0);
            this.richReceive.Name = "richReceive";
            this.richReceive.ReadOnly = true;
            this.richReceive.Size = new System.Drawing.Size(1937, 716);
            this.richReceive.TabIndex = 0;
            this.richReceive.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 125);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 32);
            this.label1.TabIndex = 1;
            this.label1.Text = "send";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.richSend);
            this.panel2.Location = new System.Drawing.Point(21, 159);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1941, 321);
            this.panel2.TabIndex = 2;
            // 
            // richSend
            // 
            this.richSend.BackColor = System.Drawing.SystemColors.Info;
            this.richSend.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richSend.Location = new System.Drawing.Point(0, 0);
            this.richSend.Name = "richSend";
            this.richSend.ReadOnly = true;
            this.richSend.Size = new System.Drawing.Size(1937, 317);
            this.richSend.TabIndex = 0;
            this.richSend.Text = "";
            // 
            // comboBoxComPort
            // 
            this.comboBoxComPort.FormattingEnabled = true;
            this.comboBoxComPort.Location = new System.Drawing.Point(21, 73);
            this.comboBoxComPort.Name = "comboBoxComPort";
            this.comboBoxComPort.Size = new System.Drawing.Size(312, 39);
            this.comboBoxComPort.TabIndex = 1;
            this.comboBoxComPort.DropDown += new System.EventHandler(this.comboBoxComPort_DropDown);
            // 
            // btnConnect
            // 
            this.btnConnect.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnConnect.Location = new System.Drawing.Point(21, 0);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(312, 67);
            this.btnConnect.TabIndex = 0;
            this.btnConnect.Text = "connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // panelLog
            // 
            this.panelLog.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelLog.Controls.Add(this.btnStartLog);
            this.panelLog.Controls.Add(this.groupBox1);
            this.panelLog.Controls.Add(this.tbPath);
            this.panelLog.Enabled = false;
            this.panelLog.Location = new System.Drawing.Point(12, 1370);
            this.panelLog.Name = "panelLog";
            this.panelLog.Size = new System.Drawing.Size(2601, 151);
            this.panelLog.TabIndex = 3;
            // 
            // btnStartLog
            // 
            this.btnStartLog.Location = new System.Drawing.Point(1861, 61);
            this.btnStartLog.Name = "btnStartLog";
            this.btnStartLog.Size = new System.Drawing.Size(132, 56);
            this.btnStartLog.TabIndex = 4;
            this.btnStartLog.Text = "start";
            this.btnStartLog.UseVisualStyleBackColor = true;
            this.btnStartLog.Click += new System.EventHandler(this.btnStartLog_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdRewrite);
            this.groupBox1.Controls.Add(this.rdAppend);
            this.groupBox1.Location = new System.Drawing.Point(2033, 33);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(522, 100);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            // 
            // rdRewrite
            // 
            this.rdRewrite.AutoSize = true;
            this.rdRewrite.Location = new System.Drawing.Point(327, 38);
            this.rdRewrite.Name = "rdRewrite";
            this.rdRewrite.Size = new System.Drawing.Size(137, 36);
            this.rdRewrite.TabIndex = 0;
            this.rdRewrite.Text = "rewrite";
            this.rdRewrite.UseVisualStyleBackColor = true;
            // 
            // rdAppend
            // 
            this.rdAppend.AutoSize = true;
            this.rdAppend.Checked = true;
            this.rdAppend.Location = new System.Drawing.Point(27, 38);
            this.rdAppend.Name = "rdAppend";
            this.rdAppend.Size = new System.Drawing.Size(151, 36);
            this.rdAppend.TabIndex = 0;
            this.rdAppend.TabStop = true;
            this.rdAppend.Text = "Append";
            this.rdAppend.UseVisualStyleBackColor = true;
            // 
            // tbPath
            // 
            this.tbPath.Location = new System.Drawing.Point(13, 74);
            this.tbPath.Name = "tbPath";
            this.tbPath.ReadOnly = true;
            this.tbPath.Size = new System.Drawing.Size(1819, 38);
            this.tbPath.TabIndex = 0;
            // 
            // SerialPort
            // 
            this.SerialPort.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.SerialPort_DataReceived);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panel10);
            this.panel4.Controls.Add(this.panel9);
            this.panel4.Controls.Add(this.panel8);
            this.panel4.Controls.Add(this.panel7);
            this.panel4.Controls.Add(this.panel11);
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Location = new System.Drawing.Point(1994, 38);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(613, 1263);
            this.panel4.TabIndex = 3;
            // 
            // panel10
            // 
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel10.Controls.Add(this.lblSub);
            this.panel10.Controls.Add(this.label14);
            this.panel10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.panel10.Location = new System.Drawing.Point(51, 1029);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(522, 143);
            this.panel10.TabIndex = 0;
            // 
            // lblSub
            // 
            this.lblSub.AutoSize = true;
            this.lblSub.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblSub.ForeColor = System.Drawing.Color.Blue;
            this.lblSub.Location = new System.Drawing.Point(316, 48);
            this.lblSub.Name = "lblSub";
            this.lblSub.Size = new System.Drawing.Size(29, 39);
            this.lblSub.TabIndex = 1;
            this.lblSub.Text = "-";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.DarkMagenta;
            this.label14.Location = new System.Drawing.Point(41, 55);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(195, 32);
            this.label14.TabIndex = 0;
            this.label14.Text = "sub Reading:";
            // 
            // panel9
            // 
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel9.Controls.Add(this.lblMain);
            this.panel9.Controls.Add(this.label12);
            this.panel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.panel9.Location = new System.Drawing.Point(51, 817);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(522, 143);
            this.panel9.TabIndex = 0;
            this.panel9.Paint += new System.Windows.Forms.PaintEventHandler(this.panel9_Paint);
            // 
            // lblMain
            // 
            this.lblMain.AutoSize = true;
            this.lblMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblMain.ForeColor = System.Drawing.Color.Blue;
            this.lblMain.Location = new System.Drawing.Point(316, 48);
            this.lblMain.Name = "lblMain";
            this.lblMain.Size = new System.Drawing.Size(29, 39);
            this.lblMain.TabIndex = 1;
            this.lblMain.Text = "-";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.DarkMagenta;
            this.label12.Location = new System.Drawing.Point(41, 55);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(212, 32);
            this.label12.TabIndex = 0;
            this.label12.Text = "Main Reading:";
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel8.Controls.Add(this.lblRange);
            this.panel8.Controls.Add(this.label10);
            this.panel8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.panel8.Location = new System.Drawing.Point(51, 626);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(522, 143);
            this.panel8.TabIndex = 0;
            this.panel8.Paint += new System.Windows.Forms.PaintEventHandler(this.panel8_Paint);
            // 
            // lblRange
            // 
            this.lblRange.AutoSize = true;
            this.lblRange.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblRange.ForeColor = System.Drawing.Color.Blue;
            this.lblRange.Location = new System.Drawing.Point(316, 48);
            this.lblRange.Name = "lblRange";
            this.lblRange.Size = new System.Drawing.Size(29, 39);
            this.lblRange.TabIndex = 1;
            this.lblRange.Text = "-";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.DarkMagenta;
            this.label10.Location = new System.Drawing.Point(41, 55);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(193, 32);
            this.label10.TabIndex = 0;
            this.label10.Text = "Range Code:";
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel7.Controls.Add(this.lblKey);
            this.panel7.Controls.Add(this.label8);
            this.panel7.Location = new System.Drawing.Point(51, 440);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(522, 143);
            this.panel7.TabIndex = 0;
            this.panel7.Paint += new System.Windows.Forms.PaintEventHandler(this.panel7_Paint);
            // 
            // lblKey
            // 
            this.lblKey.AutoSize = true;
            this.lblKey.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblKey.ForeColor = System.Drawing.Color.Blue;
            this.lblKey.Location = new System.Drawing.Point(316, 48);
            this.lblKey.Name = "lblKey";
            this.lblKey.Size = new System.Drawing.Size(29, 39);
            this.lblKey.TabIndex = 1;
            this.lblKey.Text = "-";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label8.ForeColor = System.Drawing.Color.DarkMagenta;
            this.label8.Location = new System.Drawing.Point(41, 55);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(156, 32);
            this.label8.TabIndex = 0;
            this.label8.Text = "Key Code:";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // panel11
            // 
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel11.Controls.Add(this.lblRotor);
            this.panel11.Controls.Add(this.label5);
            this.panel11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.panel11.Location = new System.Drawing.Point(51, 91);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(522, 143);
            this.panel11.TabIndex = 0;
            this.panel11.Paint += new System.Windows.Forms.PaintEventHandler(this.panel6_Paint);
            // 
            // lblRotor
            // 
            this.lblRotor.AutoSize = true;
            this.lblRotor.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblRotor.ForeColor = System.Drawing.Color.Blue;
            this.lblRotor.Location = new System.Drawing.Point(316, 48);
            this.lblRotor.Name = "lblRotor";
            this.lblRotor.Size = new System.Drawing.Size(29, 39);
            this.lblRotor.TabIndex = 1;
            this.lblRotor.Text = "-";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.DarkMagenta;
            this.label5.Location = new System.Drawing.Point(41, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(178, 32);
            this.label5.TabIndex = 0;
            this.label5.Text = "Rotor Code:";
            this.label5.Click += new System.EventHandler(this.label6_Click);
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel6.Controls.Add(this.lblBlue);
            this.panel6.Controls.Add(this.label6);
            this.panel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.panel6.Location = new System.Drawing.Point(51, 271);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(522, 143);
            this.panel6.TabIndex = 0;
            this.panel6.Paint += new System.Windows.Forms.PaintEventHandler(this.panel6_Paint);
            // 
            // lblBlue
            // 
            this.lblBlue.AutoSize = true;
            this.lblBlue.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblBlue.ForeColor = System.Drawing.Color.Blue;
            this.lblBlue.Location = new System.Drawing.Point(316, 48);
            this.lblBlue.Name = "lblBlue";
            this.lblBlue.Size = new System.Drawing.Size(29, 39);
            this.lblBlue.TabIndex = 1;
            this.lblBlue.Text = "-";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.DarkMagenta;
            this.label6.Location = new System.Drawing.Point(41, 55);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(166, 32);
            this.label6.TabIndex = 0;
            this.label6.Text = "Blue Code:";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // cbLog
            // 
            this.cbLog.AutoSize = true;
            this.cbLog.Location = new System.Drawing.Point(35, 1328);
            this.cbLog.Name = "cbLog";
            this.cbLog.Size = new System.Drawing.Size(101, 36);
            this.cbLog.TabIndex = 2;
            this.cbLog.Text = "Log";
            this.cbLog.UseVisualStyleBackColor = true;
            this.cbLog.CheckedChanged += new System.EventHandler(this.cbLog_CheckedChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(2625, 1571);
            this.Controls.Add(this.panelLog);
            this.Controls.Add(this.cbLog);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.Text = "APPA 109N";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panelLog.ResumeLayout(false);
            this.panelLog.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnConnect;
        private System.IO.Ports.SerialPort SerialPort;
        private System.Windows.Forms.ComboBox comboBoxComPort;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RichTextBox richSend;
        private System.Windows.Forms.RichTextBox richReceive;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnClearSend;
        private System.Windows.Forms.Button btnClearReceive;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label lblBlue;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label lblKey;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label lblRange;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label lblMain;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label lblSub;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbInterval;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label lblRotor;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panelLog;
        private System.Windows.Forms.TextBox tbPath;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.CheckBox cbLog;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdRewrite;
        private System.Windows.Forms.RadioButton rdAppend;
        private System.Windows.Forms.Button btnStartLog;
    }
}

