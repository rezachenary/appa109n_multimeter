﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Threading;
using System.Diagnostics;
using System.IO;

namespace APPA_109N
{
    public partial class MainForm : Form
    {
        Thread ThreadSending;
        Boolean isRun = false;
        int Interval = 1000;
        bool setLog = false;
        public MainForm()
        {
            InitializeComponent();
        }
        private void SearchPort()
        {
            string[] ArrayComPortsNames = null;
            int index = -1;
            string ComPortName = null;
            comboBoxComPort.Text = "";
            comboBoxComPort.Items.Clear();
            ArrayComPortsNames = SerialPort.GetPortNames();
            if (ArrayComPortsNames.Length > 0)
            {
                do
                {
                    index += 1;
                    comboBoxComPort.Items.Add(ArrayComPortsNames[index]);
                }
                while (!((ArrayComPortsNames[index] == ComPortName)
                              || (index == ArrayComPortsNames.GetUpperBound(0))));
                Array.Sort(ArrayComPortsNames);

                //want to get first out
                if (index == ArrayComPortsNames.GetUpperBound(0))
                {
                    ComPortName = ArrayComPortsNames[0];
                }
                comboBoxComPort.Text = ArrayComPortsNames[0];
            }
        }
        private void MainForm_Load(object sender, EventArgs e)
        {
            SearchPort();
            tbInterval.Text = "" + Interval;

        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (SerialPort.IsOpen == false)
            {
                try
                {
                    if (comboBoxComPort.Text == "")
                    {
                        MessageBox.Show("Please Select Serial Port");
                        return;
                    }
                    SerialPort.PortName = comboBoxComPort.Text;
                    SerialPort.Parity = Parity.None;
                    SerialPort.StopBits = StopBits.One;
                    SerialPort.DataBits = 8;
                    SerialPort.BaudRate = 9600;
                    SerialPort.Open();
                    btnConnect.Text = "connected";
                    comboBoxComPort.Enabled = false;
                    ThreadSending = new Thread(new ThreadStart(WriteSerial));
                    ThreadSending.Start();
                }
                catch (UnauthorizedAccessException ex)
                {
                    MessageBox.Show("Error: Port {0} is in use", comboBoxComPort.Text);

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Uart exception: " + ex);
                    isRun = false;
                }
            }
            else
            {
                isRun = false;
            }

        }
        public static string ByteArrayToString(byte[] ba)
        {
            return BitConverter.ToString(ba).Replace("-", " 0x");
        }

        byte[] ReqAPPA = { 0x55, 0x55, 0x00, 0x00, 0xAA };
        private void WriteSerial()
        {
            isRun = true;
            while (isRun == true)
            {
                SerialPort.Write(ReqAPPA, 0, 5);
                String sendData = "0x" + ByteArrayToString(ReqAPPA);
                sendData += "\r\n";
                richSend.Invoke((MethodInvoker)(() =>
                {
                    richSend.Text += sendData;
                    richSend.SelectionStart = richSend.Text.Length;
                    richSend.ScrollToCaret();
                }));
                Thread.Sleep(Interval);
            }
            btnConnect.Invoke((MethodInvoker)(() => btnConnect.Text = "disconnected"));
            SerialPort.Close();
            comboBoxComPort.Invoke((MethodInvoker)(() => comboBoxComPort.Enabled = true));
            ThreadSending.Abort();
            setLog = false;
        }


        private void comboBoxComPort_DropDown(object sender, EventArgs e)
        {
            SearchPort();
        }

        private bool checkCRC(byte[] InputData)
        {
            int sum = 0;
            for (int i = 0; i < (InputData.Length - 1); i++)
            {
                sum += InputData[i];
            }
            if ((byte)sum == InputData[InputData.Length - 1])
            {
                return true;
            }
            return false;
        }



        private void btnClearSend_Click(object sender, EventArgs e)
        {
            richSend.Text = "";
        }

        private void btnClearReceive_Click(object sender, EventArgs e)
        {
            richReceive.Text = "";
        }

        private void panel9_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel8_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void panel6_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel7_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            Interval = Convert.ToInt32(tbInterval.Text);
        }

        private void tbInterval_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar)
                    && !char.IsDigit(e.KeyChar)
                    && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if (e.KeyChar == '.'
                && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
        }




        String fileContent = string.Empty;
        String filePath = string.Empty;



        private void cbLog_CheckedChanged(object sender, EventArgs e)
        {
            if (cbLog.Checked == true)
            {
                panelLog.Enabled = true;
            }
            else
            {
                panelLog.Enabled = false;
                setLog = false;
            }
        }

        private void btnStartLog_Click(object sender, EventArgs e)
        {
            if (tbPath.Text == "")
            {
                saveFileDialog1.InitialDirectory = @"C:\";
                saveFileDialog1.Title = "Save csv Files";
                saveFileDialog1.CheckFileExists = false;
                saveFileDialog1.CheckPathExists = true;
                saveFileDialog1.DefaultExt = "csv";
                saveFileDialog1.Filter = "csv files (*.csv)|*.csv";
                saveFileDialog1.FilterIndex = 2;
                saveFileDialog1.RestoreDirectory = true;

                saveFileDialog1.OverwritePrompt = true;
                saveFileDialog1.CreatePrompt = false;

                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    tbPath.Text = saveFileDialog1.FileName;
                    if (rdRewrite.Checked == true)
                    {
                        System.IO.File.WriteAllText(tbPath.Text, string.Empty);
                        CreateHeader();
                    }
                    setLog = true;
                }
            }
        }


        public void CreateCSVFile(DataTable dt, string strFilePath)
        {
            StreamWriter sw = new StreamWriter(strFilePath, true);

            int iColCount = dt.Columns.Count;
            for (int i = 0; i < iColCount; i++)
            {
                sw.Write(dt.Columns[i]);
                if (i < iColCount - 1)
                {
                    sw.Write(",");
                }
            }
            sw.Write(sw.NewLine);

            foreach (DataRow dr in dt.Rows)
            {
                for (int i = 0; i < iColCount; i++)
                {
                    if (!Convert.IsDBNull(dr[i]))
                    {
                        sw.Write(dr[i].ToString());
                    }
                    if (i < iColCount - 1)
                    {
                        sw.Write(",");
                    }
                }
                sw.Write(sw.NewLine);
            }
            sw.Close();
        }

        public Boolean CreateHeader()
        {
            try
            {
                // Create the `DataTable` structure according to your data source

                DataTable table = new DataTable();
                table.Columns.Add("time", typeof(string));
                table.Columns.Add("value", typeof(String));

                //Creat CSV File
                CreateCSVFile(table, tbPath.Text);
                return true;
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.Message);
            }
        }

        public Boolean AddCsv(string DateOne, string DataTwo)
        {
            try
            {
                // Create the `DataTable` structure according to your data source

                DataTable table = new DataTable();                
                // Iterate through data source object and fill the table
                table.Columns.Add(DateOne);
                table.Columns.Add(DataTwo);
                //Creat CSV File
                CreateCSVFile(table, tbPath.Text);
                return true;
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.Message);
            }
        }

        String[] LookupRotor = { "OFF", "V", " mV ", "Ohm ", "Diode ", "mA ", "A ", "Cap. ", "Hz ", "Temp " };

        String[,] LookupBlue = {{ "AC", " AC", " Ohm", " Diode ", "AC ", "AC ", "Cap ", "Hz ", "deg.C" },
                                { "DC ", "DC ", "Low\r\nOhm ", "Beeper ", "DC ", "DC ", "-", "Duty\r\nFactor", "deg.F" },
                                { "AC+\r\nDC ", "AC+\r\nDC", "-", "-", "AC+\r\nDC", "AC+\r\nDC ", "-", "-", "-" }};
        int[] LookupPoint = { 1, 10, 100, 1000, 10000 };
        private void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            if (SerialPort.BytesToRead < 19)
                return;
            byte[] InputData = new byte[19];
            Array.Clear(InputData, 0, InputData.Length);

            SerialPort.Read(InputData, 0, InputData.Length);
            String str;
            if (checkCRC(InputData) == false)
            {
                str = "Bad CRC\r\n";
            }
            else
            {
                str = "0x" + ByteArrayToString(InputData);
                str += "\r\n";

            }
            richReceive.Invoke((MethodInvoker)(() =>
            {
                richReceive.Text += str;
                richReceive.SelectionStart = richReceive.Text.Length;
                richReceive.ScrollToCaret();

                lblRotor.Text = LookupRotor[InputData[4]];
                lblBlue.Text = LookupBlue[InputData[5], InputData[4] - 1];

                float MainCode = InputData[10] * 0x10000 + InputData[9] * 0x100 + InputData[8];
                float PointCode = LookupPoint[(InputData[11] & 0x7)];
                lblMain.Text = "" + MainCode / PointCode;

                if (setLog == true)
                {
                    AddCsv(DateTime.Now.ToString("HH:mm:ss tt"), lblMain.Text);
                }
            }));

        }

    }
}
